/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.command;

import com.oglofus.protector.OglofusProtection;
import com.oglofus.protector.api.protector.Protector;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.text.SimpleDateFormat;
import java.util.stream.Collectors;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class InfoCommand implements CommandExecutor {
    private final OglofusProtection plugin;

    public InfoCommand(OglofusProtection plugin) {
        this.plugin = plugin;
    }

    /**
     * Callback for the execution of a command.
     *
     * @param src  The commander who is executing this command
     * @param args The parsed command arguments for this command
     * @return the result of executing this command
     * @throws CommandException If a user-facing error occurs while
     *                          executing this command
     */
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        return args.<Protector>getOne(Text.of("protector"))
                .map(protector -> execute(protector, src, args))
                .orElseGet(CommandResult::empty);
    }

    private CommandResult execute(Protector protector, CommandSource source, CommandContext context) {
        Text.Builder builder = Text.builder();

        builder.append(Text.builder("Region Information:").color(TextColors.GREEN).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("   ID: ").color(TextColors.GREEN)
                        .append(Text.builder(protector.getUuid().toString()).color(TextColors.RESET).build()).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("   Owner(s): ").color(TextColors.GREEN)
                        .append(Text.builder(StringUtils.join(protector.getStaff()
                                .getOwners()
                                .stream()
                                .map(User::getName)
                                .collect(Collectors.toList()), ", "))
                                .color(TextColors.RESET)
                                .build()).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("   Officer(s): ").color(TextColors.GREEN)
                        .append(Text.builder(StringUtils.join(protector.getStaff()
                                .getOfficers()
                                .stream()
                                .map(User::getName)
                                .collect(Collectors.toList()), ", "))
                                .color(TextColors.RESET)
                                .build()).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("   Member(s): ").color(TextColors.GREEN)
                        .append(Text.builder(StringUtils.join(protector.getStaff()
                                .getMembers()
                                .stream()
                                .map(User::getName)
                                .collect(Collectors.toList()), ", "))
                                .color(TextColors.RESET)
                                .build()).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("   Created on: ").color(TextColors.GREEN)
                        .append(Text.builder(new SimpleDateFormat("yyyy-MM-dd").format(protector.getCreated()))
                                .color(TextColors.RESET)
                                .build()).build());

        source.sendMessage(builder.build());

        return CommandResult.success();
    }
}
