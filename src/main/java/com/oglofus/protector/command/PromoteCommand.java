/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.command;

import com.oglofus.protector.OglofusProtection;
import com.oglofus.protector.api.protector.Protector;
import com.oglofus.protector.api.protector.user.Rank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 18/05/2017.
 */
public class PromoteCommand implements CommandExecutor {
    private final OglofusProtection plugin;

    public PromoteCommand(OglofusProtection plugin) {
        this.plugin = plugin;
    }

    /**
     * Callback for the execution of a command.
     *
     * @param src  The commander who is executing this command
     * @param args The parsed command arguments for this command
     * @return the result of executing this command
     * @throws CommandException If a user-facing error occurs while
     *                          executing this command
     */
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        return args.<Protector>getOne(Text.of("protector"))
                .map(protector -> execute(protector, src, args))
                .orElseGet(CommandResult::empty);
    }

    private CommandResult execute(Protector protector, CommandSource source, CommandContext context) {
        User target = context.<User>getOne(Text.of("player")).get();

        if (source instanceof Player) {
            Player player = (Player) source;


            if (target.getUniqueId().equals(player.getUniqueId())) {
                player.sendMessage(Text.of(
                        TextColors.BLUE, "U ",
                        TextColors.GOLD, "mad ",
                        TextColors.DARK_AQUA, "bro? ",
                        TextColors.RED, "Of course, you can't promote yourself."
                ));

                return CommandResult.empty();
            }

            if (!(protector.getStaff().isOwner(player) || player.hasPermission("oglofus.protection.bypass.promote"))) {
                player.sendMessage(Text.of(
                        TextColors.RED,
                        "You don't have the right to promote this player."
                ));

                return CommandResult.empty();
            }
        }

        switch (protector.getStaff().getRank(target)) {
            case Owner:
                source.sendMessage(Text.of(
                        TextColors.GREEN,
                        "You make him GOD. Well done. ;)"
                ));

                return CommandResult.empty();
            case Member:
                protector.getStaff().setRank(target, Rank.Officer);

                source.sendMessage(Text.of(
                        TextColors.GREEN,
                        "You have successfully promoted him to Officer."
                ));

                return CommandResult.success();
            case Officer:
                protector.getStaff().setRank(target, Rank.Owner);

                source.sendMessage(Text.of(
                        TextColors.GREEN,
                        "You have successfully promoted him to Owner."
                ));

                return CommandResult.success();
            case None:
                source.sendMessage(Text.of(
                        TextColors.RED,
                        "The player must be a member of the region to promote him."
                ));

                return CommandResult.empty();
        }


        return CommandResult.success();
    }
}
