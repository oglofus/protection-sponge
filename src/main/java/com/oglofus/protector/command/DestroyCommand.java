/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.command;

import com.oglofus.protector.OglofusProtection;
import com.oglofus.protector.api.protector.Protector;
import com.oglofus.protector.protector.OglofusProtector;
import com.sk89q.worldedit.Vector;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.entity.spawn.EntitySpawnCause;
import org.spongepowered.api.event.cause.entity.spawn.SpawnTypes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class DestroyCommand implements CommandExecutor {
    private final OglofusProtection plugin;

    public DestroyCommand(OglofusProtection plugin) {
        this.plugin = plugin;
    }

    /**
     * Callback for the execution of a command.
     *
     * @param src  The commander who is executing this command
     * @param args The parsed command arguments for this command
     * @return the result of executing this command
     * @throws CommandException If a user-facing error occurs while
     *                          executing this command
     */
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        return args.<Protector>getOne(Text.of("protector"))
                .map(protector -> execute(protector, src, args))
                .orElseGet(CommandResult::empty);
    }

    private CommandResult execute(Protector protector, CommandSource source, CommandContext context) {
        if (source instanceof Player) {
            Player player = (Player) source;

            if (!(protector.getStaff().isOwner(player) || player.hasPermission("oglofus.protection.bypass.destroy"))) {
                player.sendMessage(Text.of(TextColors.RED, "You don't have the right to do that."));

                return CommandResult.empty();
            }

            if (player.getGameModeData().type().get().equals(GameModes.SURVIVAL)) {
                ItemStack.Builder builder = ItemStack.builder()
                        .itemType(ItemTypes.SPONGE)
                        .add(Keys.DISPLAY_NAME, Text.of(
                                TextColors.BLUE, "SUPER ",
                                TextColors.GOLD, "MEGA ",
                                TextColors.DARK_AQUA, "AWESOME ",
                                TextColors.AQUA, "Protection Core"))
                        .add(Keys.ITEM_LORE, Collections.singletonList(
                                Text.of("protector")
                        ));

                Location<World> location = player.getLocation();
                Extent          extent   = location.getExtent();
                Entity          item     = extent.createEntity(EntityTypes.ITEM, location.getPosition());
                ItemStack       stack    = builder.build();

                item.offer(Keys.REPRESENTED_ITEM, stack.createSnapshot());

                extent.spawnEntity(item, Cause.source(
                        EntitySpawnCause.builder()
                                .entity(item)
                                .type(SpawnTypes.PLUGIN)
                                .build()
                ).build());
            }
        }

        protector.destroy();
        plugin.protectors.remove(protector.getUuid());

        source.sendMessage(Text.of(
                TextColors.GREEN,
                "You have successfully destroyed this region."
        ));

        return CommandResult.success();
    }
}
