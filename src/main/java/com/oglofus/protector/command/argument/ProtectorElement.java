/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.command.argument;

import com.oglofus.protector.OglofusProtection;
import com.oglofus.protector.protector.OglofusProtector;
import com.sk89q.worldedit.Vector;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.ArgumentParseException;
import org.spongepowered.api.command.args.CommandArgs;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class ProtectorElement extends CommandElement {
    private final OglofusProtection plugin;

    public ProtectorElement(@Nullable Text key, OglofusProtection plugin) {
        super(key);
        this.plugin = plugin;
    }

    /**
     * Attempt to extract a value for this element from the given arguments.
     * This method is expected to have no side-effects for the source, meaning
     * that executing it will not change the state of the {@link CommandSource}
     * in any way.
     *
     * @param source The source to parse for
     * @param args   the arguments
     * @return The extracted value
     * @throws ArgumentParseException if unable to extract a value
     */
    @Nullable
    @Override
    protected Object parseValue(CommandSource source, CommandArgs args) throws ArgumentParseException {
        if (args.hasNext()) {
            try {
                UUID uuid = UUID.fromString(args.next());

                if (plugin.protectors.containsKey(uuid)) {

                    return plugin.protectors.get(uuid);
                } else {
                    source.sendMessage(Text
                            .builder("The uuid is illegal.")
                            .color(TextColors.RED)
                            .build());

                    return null;
                }

            } catch (IllegalArgumentException e) {
                source.sendMessage(Text
                        .builder("The uuid is illegal.")
                        .color(TextColors.RED)
                        .build());

                return null;
            }
        } else {
            if (source instanceof Player) {
                Player player = (Player) source;

                for (OglofusProtector protector : plugin.protectors.values()) {
                    Location<World> location = player.getLocation();
                    Vector          vector   = new Vector(location.getX(), location.getY(), location.getZ());

                    if (protector.getRegion().isTouching(vector)) return protector;
                }

                source.sendMessage(Text
                        .builder("There is no region here.")
                        .color(TextColors.RED)
                        .build());

                return null;
            } else {

                source.sendMessage(Text
                        .builder("You must be a player to execute this command without a region id.")
                        .color(TextColors.RED)
                        .build());

                return null;
            }
        }
    }

    /**
     * Fetch completions for command arguments.
     *
     * @param src     The source requesting tab completions
     * @param args    The arguments currently provided
     * @param context The context to store state in
     * @return Any relevant completions
     */
    @Override
    public List<String> complete(CommandSource src, CommandArgs args, CommandContext context) {
        return Collections.emptyList();
    }

    @Override
    public Text getUsage(CommandSource src) {
        return Text.of("[uuid]");
    }
}
