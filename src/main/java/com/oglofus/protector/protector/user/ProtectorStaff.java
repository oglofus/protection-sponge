/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.protector.user;

import com.oglofus.protector.api.protector.user.Rank;
import com.oglofus.protector.api.protector.user.Staff;
import com.oglofus.protector.protector.OglofusProtector;
import org.spongepowered.api.entity.living.player.User;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class ProtectorStaff implements Staff {
    private final OglofusProtector protector;
    private final Map<User, Rank>  staff;

    public ProtectorStaff(OglofusProtector protector, Map<User, Rank> staff) {
        this.protector = protector;
        this.staff = staff;
    }

    @Override
    public Rank getRank(User user) {
        return staff.getOrDefault(user, Rank.None);
    }

    @Override
    public void setRank(User user, Rank rank) {
        if (rank.equals(Rank.None)) {
            staff.remove(user);
        } else {
            staff.put(user, rank);
        }
    }

    @Override
    public Collection<User> getOwners() {
        return staff.keySet().stream().filter(this::isOwner).collect(Collectors.toSet());
    }

    @Override
    public Collection<User> getMembers() {
        return staff.keySet().stream().filter(this::isMember).collect(Collectors.toSet());
    }

    @Override
    public Collection<User> getOfficers() {
        return staff.keySet().stream().filter(this::isOfficer).collect(Collectors.toSet());
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<User> iterator() {
        return staff.keySet().iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProtectorStaff)) return false;

        ProtectorStaff users = (ProtectorStaff) o;

        if (!protector.equals(users.protector)) return false;
        return staff.equals(users.staff);
    }

    @Override
    public int hashCode() {
        int result = protector.hashCode();
        result = 31 * result + staff.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ProtectorStaff{" +
                "protector=" + protector +
                ", staff=" + staff +
                '}';
    }
}
