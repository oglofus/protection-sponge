/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.protector.region;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.Lists;
import com.oglofus.protector.api.protector.region.Effects;
import com.oglofus.protector.api.protector.region.Region;
import com.oglofus.protector.protector.OglofusProtector;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.scheduler.Task;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class ProtectorEffects implements Effects {
    private final List<Task> effects = Lists.newArrayList();

    private final OglofusProtector protector;
    private final Region           region;
    private final Task             refresher;
    private       boolean          enabled;

    public ProtectorEffects(OglofusProtector protector, boolean enabled) {
        this.protector = protector;
        this.region = protector.getRegion();
        this.refresher = Sponge.getScheduler()
                .createTaskBuilder()
                .async()
                .interval(1, TimeUnit.MINUTES)
                .execute(this::refreshEffects)
                .submit(protector.plugin);
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void stopEffects() {
        effects.forEach(Task::cancel);
    }

    @Override
    public void playBorderEffect() {
        if (!(effects.isEmpty() || enabled)) return;

        region.getSmall().getWalls().forEach(blockVector -> {
            Vector3d position = new Vector3d(
                    blockVector.getX() + 0.5,
                    blockVector.getY(),
                    blockVector.getZ() + 0.5
            );

            if (!region.getCenter().getExtent().getLocation(position).getBlock().getType().equals
                    (BlockTypes.AIR))
                return;

            if (region.getCenter().getExtent().getLocation(position.add(0, -1, 0)).getBlock()
                    .getType().equals
                            (BlockTypes.AIR))
                return;

            ParticleEffect effect = ParticleEffect.builder()
                    .type(ParticleTypes.HAPPY_VILLAGER)
                    .quantity(1)
                    .build();

            effects.add(Sponge.getScheduler()
                    .createTaskBuilder()
                    .async()
                    .interval(5, TimeUnit.SECONDS)
                    .execute(() -> region.getCenter().getExtent().spawnParticles(effect, position))
                    .submit(protector.plugin));
        });
    }

    @Override
    public void playCreatingEffect() {
        region.getSmall().forEach(blockVector -> {
            Vector3d position = new Vector3d(
                    blockVector.getX() + 0.5,
                    blockVector.getY(),
                    blockVector.getZ() + 0.5
            );

            if (!region.getCenter().getExtent().getLocation(position).getBlock().getType().equals
                    (BlockTypes.AIR))
                return;

            if (region.getCenter().getExtent().getLocation(position.add(0, -5, 0)).getBlock()
                    .getType().equals
                            (BlockTypes.AIR))
                return;

            ParticleEffect effect = ParticleEffect.builder()
                    .type(ParticleTypes.CLOUD)
                    .quantity(1)
                    .build();

            Sponge.getScheduler()
                    .createTaskBuilder()
                    .async()
                    .delay(ThreadLocalRandom.current().nextInt(0, 500 + 1), TimeUnit.MILLISECONDS)
                    .execute(() -> region.getCenter().getExtent().spawnParticles(effect, position))
                    .submit(protector.plugin);
        });
    }

    @Override
    public void playDestroyingEffect() {
        region.getSmall().forEach(blockVector -> {
            Vector3d position = new Vector3d(
                    blockVector.getX() + 0.5,
                    blockVector.getY(),
                    blockVector.getZ() + 0.5
            );

            if (!region.getCenter().getExtent().getLocation(position).getBlock().getType().equals
                    (BlockTypes.AIR))
                return;

            if (region.getCenter().getExtent().getLocation(position.add(0, -5, 0)).getBlock()
                    .getType().equals
                            (BlockTypes.AIR))
                return;

            ParticleEffect effect = ParticleEffect.builder()
                    .type(ParticleTypes.FLAME)
                    .quantity(1)
                    .build();

            Sponge.getScheduler()
                    .createTaskBuilder()
                    .async()
                    .delay(ThreadLocalRandom.current().nextInt(0, 500 + 1), TimeUnit.MILLISECONDS)
                    .execute(() -> region.getCenter().getExtent().spawnParticles(effect, position))
                    .submit(protector.plugin);
        });
    }

    @Override
    public void setEnable(boolean enable) {
        if (this.enabled == enable) return;

        this.enabled = enable;

        if (!enable) {
            stopEffects();
        } else {
            playBorderEffect();
        }
    }

    @Override
    public void refreshEffects() {
        stopEffects();
        playBorderEffect();
    }

    @Override
    public Task getRefresher() {
        return refresher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProtectorEffects)) return false;

        ProtectorEffects that = (ProtectorEffects) o;

        if (isEnabled() != that.isEnabled()) return false;
        if (!effects.equals(that.effects)) return false;
        if (!protector.equals(that.protector)) return false;
        return region.equals(that.region);
    }

    @Override
    public int hashCode() {
        int result = effects.hashCode();
        result = 31 * result + protector.hashCode();
        result = 31 * result + region.hashCode();
        result = 31 * result + (isEnabled() ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProtectorEffects{" +
                "effects=" + effects +
                ", protector=" + protector +
                ", region=" + region +
                ", enabled=" + enabled +
                '}';
    }
}
