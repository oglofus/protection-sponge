/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.protector.region;

import com.oglofus.protector.protector.OglofusProtector;
import com.sk89q.worldedit.BlockVector;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class ProtectorListener {
    private final OglofusProtector protector;

    public ProtectorListener(OglofusProtector protector) {
        this.protector = protector;
    }

    @Listener(order = Order.BEFORE_POST)
    public void secure(InteractBlockEvent event) {
        BlockSnapshot snapshot = event.getTargetBlock();

        snapshot.getLocation().ifPresent(location -> {
            BlockVector vector = new BlockVector(
                    location.getX(),
                    location.getY(),
                    location.getZ()
            );

            if (protector.getRegion().isTouching(vector)) {

                Optional<Player> playerOptional = event.getCause().last(Player.class);

                if (playerOptional.isPresent()) {
                    Player player = playerOptional.get();

                    if (!(protector.getStaff().isStaff(player) ||
                            player.hasPermission("oglofus.protection.bypass.use"))) {

                        ParticleEffect effect = ParticleEffect.builder()
                                .type(ParticleTypes.FLAME)
                                .quantity(1)
                                .build();

                        Sponge.getScheduler()
                                .createTaskBuilder()
                                .async()
                                .delay(ThreadLocalRandom.current().nextInt(0, 500 + 1), TimeUnit.MILLISECONDS)
                                .execute(() -> location.getExtent().spawnParticles(
                                        effect, location.getPosition().add(0.25, 1, 0.25)
                                ))
                                .submit(protector.plugin);

                        event.setCancelled(true);
                    }

                    return;
                }

                event.setCancelled(true);
            }
        });
    }

    @Listener(order = Order.BEFORE_POST)
    public void destroy(ChangeBlockEvent.Break event) {
        event.getTransactions().forEach(transaction -> {
            BlockSnapshot snapshot = transaction.getOriginal();

            snapshot.getLocation().ifPresent(location -> {
                if (location.equals(protector.getRegion().getCenter())) {

                    Optional<Player> playerOptional = event.getCause().last(Player.class);

                    if (playerOptional.isPresent()) {
                        Player player = playerOptional.get();

                        if (!(protector.getStaff().isOwner(player) || player.hasPermission("oglofus.protection.bypass" +
                                ".destroy"))) {

                            player.sendMessage(Text.of(TextColors.RED, "You don't have the right to do that."));
                        } else {
                            player.sendMessage(Text.of(
                                    TextColors.RED,
                                    "You can't destroy a region like that.",
                                    Text.NEW_LINE,
                                    TextColors.RED,
                                    "You must use the command ",
                                    TextColors.RESET,
                                    Text.builder("/p destroy")
                                            .onClick(TextActions.suggestCommand("/p destroy"))
                                            .onHover(TextActions.showText(
                                                    Text.of("Click to suggest the command.")
                                            ))
                                            .style(TextStyles.UNDERLINE)
                                            .build(),
                                    TextColors.RED,
                                    " if you want to destroy this region."
                            ));
                        }
                    }

                    transaction.setValid(false);
                }
            });
        });
    }

    @Listener(order = Order.BEFORE_POST)
    public void secure(ChangeBlockEvent event) {
        event.getTransactions().forEach(transaction -> {
            BlockSnapshot snapshot = transaction.getOriginal();

            snapshot.getLocation().ifPresent(location -> {
                BlockVector vector = new BlockVector(
                        location.getX(),
                        location.getY(),
                        location.getZ()
                );

                if (protector.getRegion().isTouching(vector)) {
                    Optional<Player> playerOptional = event.getCause().last(Player.class);

                    if (playerOptional.isPresent()) {
                        Player player = playerOptional.get();

                        if (!(protector.getStaff().isStaff(player) ||
                                player.hasPermission("oglofus.protection.bypass.change"))) {

                            player.sendMessage(Text.of(TextColors.RED, "You don't have the right to do that."));

                            transaction.setValid(false);
                        }

                        return;
                    }

                    transaction.setValid(false);
                }
            });
        });
    }
}
