/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.protector.region;

import com.oglofus.protector.api.protector.region.Region;
import com.oglofus.protector.protector.OglofusProtector;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.regions.CuboidRegion;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Iterator;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class ProtectorRegion implements Region {
    private final OglofusProtector protector;
    private final Integer          radius;
    private final CuboidRegion     small;
    private final CuboidRegion     vectors;
    private final Location<World>  center;

    public ProtectorRegion(OglofusProtector protector, Integer radius, Location<World> center) {
        this.protector = protector;
        this.radius = radius;
        this.center = center;

        this.vectors = new CuboidRegion(new BlockVector(
                center.getX() - radius,
                0,
                center.getZ() - radius
        ), new BlockVector(
                center.getX() + radius,
                center.getExtent().getBlockMax().getY(),
                center.getZ() + radius
        ));

        this.small = CuboidRegion.fromCenter(new BlockVector(center.getX(), center.getY(), center.getZ()), radius);
    }

    @Override
    public Integer getRadius() {
        return radius;
    }

    @Override
    public CuboidRegion getSmall() {
        return small;
    }

    @Override
    public CuboidRegion getVectors() {
        return vectors;
    }

    @Override
    public Location<World> getCenter() {
        return center;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<BlockVector> iterator() {
        return vectors.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProtectorRegion)) return false;

        ProtectorRegion that = (ProtectorRegion) o;

        if (!protector.equals(that.protector)) return false;
        if (!getRadius().equals(that.getRadius())) return false;
        if (!getSmall().equals(that.getSmall())) return false;
        if (!getVectors().equals(that.getVectors())) return false;
        return getCenter().equals(that.getCenter());
    }

    @Override
    public int hashCode() {
        int result = protector.hashCode();
        result = 31 * result + getRadius().hashCode();
        result = 31 * result + getSmall().hashCode();
        result = 31 * result + getVectors().hashCode();
        result = 31 * result + getCenter().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ProtectorRegion{" +
                "protector=" + protector +
                ", radius=" + radius +
                ", small=" + small +
                ", vectors=" + vectors +
                ", center=" + center +
                '}';
    }
}
