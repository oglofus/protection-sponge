/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.protector;

import com.oglofus.protector.OglofusProtection;
import com.oglofus.protector.api.protector.Protector;
import com.oglofus.protector.api.protector.region.Effects;
import com.oglofus.protector.api.protector.region.Region;
import com.oglofus.protector.api.protector.user.Rank;
import com.oglofus.protector.api.protector.user.Staff;
import com.oglofus.protector.protector.region.ProtectorEffects;
import com.oglofus.protector.protector.region.ProtectorListener;
import com.oglofus.protector.protector.region.ProtectorRegion;
import com.oglofus.protector.protector.user.ProtectorStaff;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public class OglofusProtector implements Protector {
    public final OglofusProtection plugin;

    private final UUID              uuid;
    private final ProtectorStaff    staff;
    private final Date              create;
    private final ProtectorRegion   region;
    private final ProtectorEffects  effects;
    private final ProtectorListener listener;

    public OglofusProtector(OglofusProtection plugin, UUID uuid, Map<User, Rank> staff, Date create, Integer radius,
                            Location<World> center, boolean allowEffects) {
        this.plugin = plugin;
        this.uuid = uuid;
        this.staff = new ProtectorStaff(this, staff);
        this.create = create;
        this.region = new ProtectorRegion(this, radius, center);
        this.effects = new ProtectorEffects(this, allowEffects);
        this.listener = new ProtectorListener(this);

        Sponge.getEventManager().registerListeners(plugin, this.listener);
    }

    @Override
    public UUID getUuid() {
        return uuid;
    }

    @Override
    public Staff getStaff() {
        return staff;
    }

    @Override
    public Date getCreated() {
        return create;
    }

    @Override
    public Region getRegion() {
        return region;
    }

    @Override
    public Effects getEffects() {
        return effects;
    }

    @Override
    public void destroy() {
        Sponge.getEventManager().unregisterListeners(this.listener);
        this.effects.getRefresher().cancel();
        this.effects.stopEffects();

        region.getCenter().removeBlock(Cause.source(
                Sponge.getPluginManager()
                        .getPlugin("protection")
                        .get()
        ).build());

        region.getCenter().getExtent().spawnParticles(
                ParticleEffect.builder()
                        .type(ParticleTypes.BREAK_BLOCK)
                        .quantity(1)
                        .build(),
                region.getCenter()
                        .getPosition()
                        .add(0.25, 0, 0.25)
        );

        this.effects.playDestroyingEffect();
    }
}
