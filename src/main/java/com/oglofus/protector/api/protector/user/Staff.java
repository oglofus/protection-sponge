/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.api.protector.user;

import org.spongepowered.api.entity.living.player.User;

import java.util.Collection;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public interface Staff extends Iterable<User> {
    Rank getRank(User user);

    void setRank(User user, Rank rank);

    default boolean isStaff(User user) {
        return !getRank(user).equals(Rank.None);
    }

    default boolean isOwner(User user) {
        return getRank(user).equals(Rank.Owner);
    }

    default boolean isMember(User user) {
        return getRank(user).equals(Rank.Member);
    }

    default boolean isOfficer(User user) {
        return getRank(user).equals(Rank.Officer);
    }

    Collection<User> getOwners();

    Collection<User> getMembers();

    Collection<User> getOfficers();
}
