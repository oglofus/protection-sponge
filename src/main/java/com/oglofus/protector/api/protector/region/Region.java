/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.api.protector.region;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.regions.CuboidRegion;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public interface Region extends Iterable<BlockVector> {
    Integer getRadius();

    CuboidRegion getSmall();

    CuboidRegion getVectors();

    Location<World> getCenter();

    default boolean isTouching(CuboidRegion region) {
        Vector pos1 = region.getPos1();
        Vector pos2 = region.getPos2();

        //Fast check. Checking the corners.
        if (isTouching(pos1)
                || isTouching(pos2)
                || isTouching(new Vector(pos1.getBlockX(), pos2.getBlockY(), pos1.getBlockZ()))
                || isTouching(new Vector(pos1.getBlockX(), pos2.getBlockY(), pos2.getBlockZ()))
                || isTouching(new Vector(pos2.getBlockX(), pos2.getBlockY(), pos1.getBlockZ()))
                || isTouching(new Vector(pos1.getBlockX(), pos1.getBlockY(), pos2.getBlockZ()))
                || isTouching(new Vector(pos2.getBlockX(), pos1.getBlockY(), pos1.getBlockZ()))
                || isTouching(new Vector(pos2.getBlockX(), pos1.getBlockY(), pos2.getBlockZ()))) {
            return true;
        }

        //Deep check. Checking all the blocks.
        for (BlockVector vector : region) {
            if (isTouching(vector)) {
                return true;
            }
        }

        return false;
    }

    default boolean isTouching(Vector vector) {
        return getVectors().contains(vector);
    }
}
