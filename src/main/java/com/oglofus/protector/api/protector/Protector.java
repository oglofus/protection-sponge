/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.api.protector;

import com.oglofus.protector.api.protector.region.Effects;
import com.oglofus.protector.api.protector.region.Region;
import com.oglofus.protector.api.protector.user.Staff;

import java.util.Date;
import java.util.UUID;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public interface Protector {
    UUID getUuid();

    Staff getStaff();

    Date getCreated();

    Region getRegion();

    Effects getEffects();

    void destroy();
}
