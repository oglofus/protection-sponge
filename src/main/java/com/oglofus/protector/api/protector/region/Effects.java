/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector.api.protector.region;


import org.spongepowered.api.scheduler.Task;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/05/2017.
 */
public interface Effects {
    boolean isEnabled();

    void stopEffects();

    void playBorderEffect();

    void playCreatingEffect();

    void playDestroyingEffect();

    void setEnable(boolean enable);

    void refreshEffects();

    Task getRefresher();
}
