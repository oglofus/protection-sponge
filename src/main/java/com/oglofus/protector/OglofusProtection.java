/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.oglofus.protector.api.protector.Protector;
import com.oglofus.protector.command.*;
import com.oglofus.protector.command.argument.ProtectorElement;
import com.oglofus.protector.protector.OglofusProtector;
import com.sk89q.worldedit.Vector;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 14/05/2017.
 */
@Plugin(
        id = "protection",
        name = "OglofusProtection",
        version = "1.0.1-SNAPSHOT",
        authors = "Nikolaos Grammatikos <nikosgram@protonmail.com>",
        description = "Make your server safest.",
        url = "https://oglofus.com/"
)
public class OglofusProtection {
    public static final List<BlockType> USABLE        = Lists.newArrayList();
    public static final List<BlockType> PENETRABLE    = Lists.newArrayList();
    public static final int             REGION_RADIUS = 6;

    static {
        USABLE.add(BlockTypes.BED);
        USABLE.add(BlockTypes.ANVIL);
        USABLE.add(BlockTypes.CHEST);
        USABLE.add(BlockTypes.HOPPER);
        USABLE.add(BlockTypes.FURNACE);
        USABLE.add(BlockTypes.DROPPER);
        USABLE.add(BlockTypes.TRAPDOOR);
        USABLE.add(BlockTypes.BIRCH_DOOR);
        USABLE.add(BlockTypes.FENCE_GATE);
        USABLE.add(BlockTypes.LIT_FURNACE);
        USABLE.add(BlockTypes.ENDER_CHEST);
        USABLE.add(BlockTypes.WOODEN_DOOR);
        USABLE.add(BlockTypes.JUNGLE_DOOR);
        USABLE.add(BlockTypes.ACACIA_DOOR);
        USABLE.add(BlockTypes.SPRUCE_DOOR);
        USABLE.add(BlockTypes.STONE_BUTTON);
        USABLE.add(BlockTypes.WOODEN_BUTTON);
        USABLE.add(BlockTypes.TRAPPED_CHEST);
        USABLE.add(BlockTypes.DARK_OAK_DOOR);
        USABLE.add(BlockTypes.RED_SHULKER_BOX);
        USABLE.add(BlockTypes.PINK_SHULKER_BOX);
        USABLE.add(BlockTypes.LIME_SHULKER_BOX);
        USABLE.add(BlockTypes.BIRCH_FENCE_GATE);
        USABLE.add(BlockTypes.BLUE_SHULKER_BOX);
        USABLE.add(BlockTypes.CYAN_SHULKER_BOX);
        USABLE.add(BlockTypes.GRAY_SHULKER_BOX);
        USABLE.add(BlockTypes.WHITE_SHULKER_BOX);
        USABLE.add(BlockTypes.ACACIA_FENCE_GATE);
        USABLE.add(BlockTypes.JUNGLE_FENCE_GATE);
        USABLE.add(BlockTypes.SPRUCE_FENCE_GATE);
        USABLE.add(BlockTypes.BLACK_SHULKER_BOX);
        USABLE.add(BlockTypes.BROWN_SHULKER_BOX);
        USABLE.add(BlockTypes.GREEN_SHULKER_BOX);
        USABLE.add(BlockTypes.ORANGE_SHULKER_BOX);
        USABLE.add(BlockTypes.PURPLE_SHULKER_BOX);
        USABLE.add(BlockTypes.SILVER_SHULKER_BOX);
        USABLE.add(BlockTypes.YELLOW_SHULKER_BOX);
        USABLE.add(BlockTypes.DARK_OAK_FENCE_GATE);
        USABLE.add(BlockTypes.MAGENTA_SHULKER_BOX);
        USABLE.add(BlockTypes.LIGHT_BLUE_SHULKER_BOX);

        PENETRABLE.add(BlockTypes.AIR);
        PENETRABLE.add(BlockTypes.LAVA);
        PENETRABLE.add(BlockTypes.WATER);
        PENETRABLE.add(BlockTypes.TALLGRASS);
        PENETRABLE.add(BlockTypes.DOUBLE_PLANT);

    }

    public final Map<UUID, OglofusProtector> protectors = Maps.newHashMap();

    @Inject
    protected Logger logger;

    @Listener
    public void onGamePreInitialization(GamePreInitializationEvent event) {
        //TODO load the regions...
    }

    @Listener
    public void onGameInitialization(GameInitializationEvent event) {
        Sponge.getEventManager().registerListeners(this, new OglofusListener(this));
    }

    @Listener
    public void onGameStarting(GameStartingServerEvent event) {
        CommandSpec give = CommandSpec.builder()
                .permission("oglofus.protection.command.give")
                .description(Text.of("Gives to the executor the Protection Core"))
                .arguments(GenericArguments.optional(GenericArguments.integer(Text.of("quantity"))))
                .executor(new GiveCommand(this))
                .build();

        CommandSpec info = CommandSpec.builder()
                .permission("oglofus.protection.command.info")
                .description(Text.of("Gives information about the region"))
                .arguments(new ProtectorElement(Text.of("protector"), this))
                .executor(new InfoCommand(this))
                .build();

        CommandSpec destroy = CommandSpec.builder()
                .permission("oglofus.protection.command.destroy")
                .description(Text.of("Destroy a protected region."))
                .arguments(new ProtectorElement(Text.of("protector"), this))
                .executor(new DestroyCommand(this))
                .build();

        CommandSpec promote = CommandSpec.builder()
                .permission("oglofus.protection.command.promote")
                .description(Text.of("Promote a player of this region."))
                .arguments(GenericArguments.onlyOne(GenericArguments.playerOrSource(Text.of("player"))),
                        new ProtectorElement(Text.of("protector"), this))
                .executor(new PromoteCommand(this))
                .build();

        CommandSpec demote = CommandSpec.builder()
                .permission("oglofus.protection.command.demote")
                .description(Text.of("Demote a player of this region."))
                .arguments(GenericArguments.onlyOne(GenericArguments.playerOrSource(Text.of("player"))),
                        new ProtectorElement(Text.of("protector"), this))
                .executor(new DemoteCommand(this))
                .build();

        CommandSpec append = CommandSpec.builder()
                .permission("oglofus.protection.command.append")
                .description(Text.of("Append a player of this region."))
                .arguments(GenericArguments.onlyOne(GenericArguments.playerOrSource(Text.of("player"))),
                        new ProtectorElement(Text.of("protector"), this))
                .executor(new AppendCommand(this))
                .build();

        CommandSpec remove = CommandSpec.builder()
                .permission("oglofus.protection.command.remove")
                .description(Text.of("Remove a player of this region."))
                .arguments(GenericArguments.onlyOne(GenericArguments.playerOrSource(Text.of("player"))),
                        new ProtectorElement(Text.of("protector"), this))
                .executor(new RemoveCommand(this))
                .build();

        CommandSpec effects = CommandSpec.builder()
                .permission("oglofus.protection.command.effects")
                .description(Text.of("Change the effects status."))
                .arguments(GenericArguments.onlyOne(GenericArguments.bool(Text.of("allow"))),
                        new ProtectorElement(Text.of("protector"), this))
                .executor(new EffectsCommand(this))
                .build();

        CommandSpec protection = CommandSpec.builder()
                .permission("oglofus.protection.command")
                .child(give, "give")
                .child(info, "info")
                .child(destroy, "destroy")
                .child(append, "append", "add", "invite")
                .child(remove, "remove", "kick")
                .child(promote, "promote")
                .child(demote, "demote")
                .child(effects, "effects")
                .build();

        Sponge.getCommandManager().register(this, protection, "protections", "protector", "protect", "p");
    }

    @Listener
    public void onGameStarted(GameStartedServerEvent event) {

    }

    protected UUID generateUuid() {
        UUID uuid = UUID.randomUUID();

        if (protectors.containsKey(uuid)) {
            return generateUuid();
        }

        return uuid;
    }
}
