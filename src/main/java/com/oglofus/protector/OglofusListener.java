/*
 * Copyright (c) 2017 Nikolaos Grammatikos <nikosgram@protonmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.oglofus.protector;

import com.google.common.collect.Maps;
import com.oglofus.protector.api.protector.Protector;
import com.oglofus.protector.api.protector.user.Rank;
import com.oglofus.protector.protector.OglofusProtector;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.regions.CuboidRegion;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 * This file is part of Oglofus Protection project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 15/05/2017.
 */
public class OglofusListener {
    private final OglofusProtection plugin;

    public OglofusListener(OglofusProtection plugin) {
        this.plugin = plugin;
    }

    @Listener(order = Order.LAST)
    public void createProtector(InteractBlockEvent.Secondary event) {
        BlockSnapshot snapshot = event.getTargetBlock();
        BlockState    state    = snapshot.getState();
        BlockType     type     = state.getType();

        event.getCause().last(Player.class).ifPresent(player -> {
            player.getItemInHand(event.getHandType()).ifPresent(item -> {
                if (item.getItem().getType().equals(ItemTypes.SPONGE)) {
                    item.get(Keys.ITEM_LORE).ifPresent(texts -> {
                        if (texts.contains(Text.of("protector"))) {
                            event.getTargetBlock().getLocation().ifPresent(location -> {
                                Optional<Boolean> sneaking = player.get(Keys.IS_SNEAKING);

                                if (sneaking.isPresent()) {
                                    if (!sneaking.get()) {
                                        if (OglofusProtection.USABLE.contains(type)) {
                                            return;
                                        }
                                    }
                                }

                                event.setCancelled(true);

                                Location<World> target = location.add(event.getTargetSide().asBlockOffset());

                                if (OglofusProtection.PENETRABLE.contains(type)) {
                                    target = location;
                                }

                                CuboidRegion region = new CuboidRegion(new BlockVector(
                                        location.getX() - 6,
                                        0,
                                        location.getZ() - 6
                                ), new BlockVector(
                                        location.getX() + 6,
                                        location.getExtent().getBlockMax().getY(),
                                        location.getZ() + 6
                                ));

                                for (Protector protector : plugin.protectors.values()) {
                                    if (protector.getRegion().isTouching(region)) {
                                        player.sendMessage(Text.of(
                                                TextColors.RED,
                                                "Sorry but is a region near to you."
                                        ));

                                        return;
                                    }
                                }

                                target.setBlock(
                                        BlockTypes.SPONGE.getDefaultState(),
                                        Cause.source(Sponge
                                                .getPluginManager()
                                                .getPlugin("protection")
                                                .get())
                                                .build()
                                );

                                if (player.getGameModeData().type().get().equals(GameModes.SURVIVAL)) {
                                    player.getInventory().query(item).poll(1);
                                }

                                Map<User, Rank> users = Maps.newHashMap();

                                users.put(player, Rank.Owner);

                                OglofusProtector protector = new OglofusProtector(
                                        plugin,
                                        plugin.generateUuid(),
                                        users,
                                        new Date(),
                                        OglofusProtection.REGION_RADIUS,
                                        target,
                                        true
                                );

                                plugin.protectors.put(protector.getUuid(), protector);
                                protector.getEffects().playCreatingEffect();
                                protector.getEffects().playBorderEffect();

                                player.sendMessage(Text.of(
                                        TextColors.GREEN,
                                        "You have successfully created your region."
                                ));
                            });
                        }
                    });
                }
            });
        });
    }
}
